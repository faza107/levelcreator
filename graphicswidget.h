#ifndef GRAPHICSWIDGET_H
#define GRAPHICSWIDGET_H

#include <QGLWidget>
#include <QtOpenGL>
#include "Maze.h"
#include "Level.h"
#include "mainwindow.h"

class GraphicsWidget : public QGLWidget
{
    Q_OBJECT
private:
    Maze *_maze;
    Level *_level;
    int _width,_height;
    QWidget *mainWindow;
public:
    void setMaze(Maze *maze);
    void setLevel(Level *level);
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void mousePressEvent(QMouseEvent *me);
    GraphicsWidget(QWidget *parent = 0);
    ~GraphicsWidget();
};

#endif
