#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "graphicswidget.h"
#include "Level.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    void setWinPos(int x, int y);
    void setStartPos(int x, int y);
    void addItem(int x, int y);
    bool isWallsSelected();
    bool isCellSelected();
private:
    Ui::MainWindow *_ui;
    Level *_level;
    QMap<QString, Level*> _ls;

private slots:
    void generate();
    void save();
    void pack();
    void log(QString text);
    void loadLevel(QString text);
};

#endif // MAINWINDOW_H
