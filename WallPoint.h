#pragma once
#include "BasicTypes.h"

class WallPoint
{
private:
	short _walls;
	int _isEmpty;
public: 
	int isStart, isEnd;
	WallPoint();
	bool isEmpty();
	void fill();
	void empty();
	int isWalled();
	bool has (Direction dir);
	void build (Direction dir);
	void unbuild (Direction dir);
};