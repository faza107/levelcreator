#include <QtOpenGL>
#include "Maze.h"
#include "Level.h"
#include "items/items.h"

void Maze::Render()
{
	dbl h = 1;
	for (int i = 0; i < _width; i++)
	{
		for (int j = 0; j < _height; j++)
		{
			if (!_maze[i][j].isEmpty())
			{
                glColor3d(0.109, 0.137, 0.168);
				glRectd(i*h, j*h, (i + 1)*h, (j + 1)*h);
			}
            glColor3d(0.18, 0.764, 0.686);
			glLineWidth(3);
            glBegin(GL_LINES);
			if (_maze[i][j].has(RIGHT))
			{
				glVertex2d(i*h, j*h);
				glVertex2d((i + 1)*h, j*h );
			}
			if (_maze[i][j].has(UP))
			{
				glVertex2d(i*h, j*h);
				glVertex2d(i*h, (j + 1)*h);
			}
			if (_maze[i][j + 1].has(RIGHT))
			{
				glVertex2d(i*h, (j + 1)*h);
				glVertex2d((i + 1)*h, (j + 1)*h);
			}
			if (_maze[i + 1][j].has(UP))
			{
				glVertex2d((i + 1)*h, j*h);
				glVertex2d((i + 1)*h, (j + 1)*h);
			}
			glEnd();
            /*if (_maze[i][j].isStart)
			{
                glColor3d(1, 0, 0);
				glRectd(i * h + h / 4, j * h + h / 4, i * h + h * 3 / 4, j * h + h * 3 / 4);
            }
			if (_maze[i][j].isEnd)
			{
                glColor3d(1, 1, 0);
				glRectd(i * h + h / 4, j * h + h / 4, i * h + h * 3 / 4, j * h + h * 3 / 4);
            }*/
		}
	}
}


void Level::render()
{
    dbl h = 1;
    _maze->Render();
    for (Item* item : items) {
        item->render();
    }
}

void Start::render() {
    double h = 1;
    int x = _x;
    int y = _y;
    glColor3d(1, 0, 0);
    glRectd(x * h + h / 4,
            y * h + h / 4,
            x * h + h * 3 / 4,
            y * h + h * 3 / 4);
}

void RotatedCell::render() {
    double h = 1;
    int x = _x;
    int y = _y;
    glColor3d(0.5, 0.5, 1);

    glRectd(x * h + h / 4,
            y * h + h / 4,
            x * h + h * 3 / 4,
            y * h + h * 3 / 4);
    glRectd(x * h + h / 2 - 0.1, y * h + h, x * h + h / 2 + 0.1, y * h);
}

void End::render() {
    double h = 1;
    int x = _x;
    int y = _y;
    glColor3d(1, 1, 0);
    glRectd(x * h + h / 4,
            y * h + h / 4,
            x * h + h * 3 / 4,
            y * h + h * 3 / 4);
}

void Button::render() {
    double h = 1;
    double addX = 0, addY = 0;
    if (dir != -1) {
        switch (dir) {
        case DIR_LEFT:
            addX = -h / 4;
            break;
        case DIR_UP:
            addY = h / 4;
            break;
        case DIR_RIGHT:
            addX = h / 4;
            break;
        case DIR_DOWN:
            addY = -h / 4;
            break;
        default:
            break;
        }
    }
    int x = _x;
    int y = _y;
    glColor3d(0, 0, 1);
    glRectd(x * h + h / 4 + addX,
            y * h + h / 4 + addY,
            x * h + h * 3 / 4 + addX,
            y * h + h * 3 / 4 + addY);
}

void HalfWall::render() {
    double h = 1;
    int x = _x;
    int y = _y;
    glBegin(GL_TRIANGLES);
    glColor3d(0, 1, 0);

    if (dir != -1) {
        switch (dir) {
        case DIR_LEFT:
            glVertex2d(x * h, y * h + h / 2);
            glVertex2d(x * h + 3 * h / 4, y * h + h / 4);
            glVertex2d(x * h + 3 * h / 4, y * h + 3 * h / 4);
            break;
        case DIR_UP:
            glVertex2d(x * h + h / 4, y * h + h / 4);
            glVertex2d(x * h + 3 * h / 4, y * h + h / 4);
            glVertex2d(x * h + h / 2, y * h + h);
            break;
        case DIR_RIGHT:
            glVertex2d(x * h + h, y * h + h / 2);
            glVertex2d(x * h + h / 4, y * h + h / 4);
            glVertex2d(x * h + h / 4, y * h + 3 * h / 4);
            break;
        case DIR_DOWN:
            glVertex2d(x * h + h / 4, y * h + 3 * h / 4);
            glVertex2d(x * h + 3 * h / 4, y * h + 3 * h / 4);
            glVertex2d(x * h + h / 2, y * h);
            break;
        default:
            break;
        }
    }
    glEnd();
}
