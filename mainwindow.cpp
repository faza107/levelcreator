#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    _ui(new Ui::MainWindow)
{
    _ui->setupUi(this);
    _level = new Level(100);
    //_ui->GLWidget->setMaze(_level->_maze);
    _ui->GLWidget->setLevel(_level);
    connect(_ui->generateButton, SIGNAL(clicked()), this, SLOT(generate()));
    connect(_ui->saveButton, SIGNAL(clicked()), this, SLOT(save()));
    connect(_ui->packButton, SIGNAL(clicked()), this, SLOT(pack()));
    connect(_ui->listWidget, SIGNAL(currentTextChanged(QString)), this, SLOT(loadLevel(QString)));

    _ui->itemsList->addItem("Remove item");
    _ui->itemsList->addItem("Start");
    _ui->itemsList->addItem("End");
    _ui->itemsList->addItem("Button");
    _ui->itemsList->addItem("Half Wall");
    _ui->itemsList->addItem("Rotated Cell");
    _ui->itemsList->addItem("Change Walls");
    _ui->itemsList->addItem("Add cell");

    _ui->nextItemId->setText("Id will be here");
}

void MainWindow::generate()
{
    //delete _level;
    int count = _ui->countSpinBox->value();
    _level = new Level(count);
    //_ui->GLWidget->setMaze(_level->_maze);
    _ui->GLWidget->setLevel(_level);
    _ui->GLWidget->updateGL();
}

void MainWindow::log(QString text)
{
    QTextCursor cursor = _ui->log->textCursor();
    cursor.setPosition(0);

    _ui->log->setTextCursor(cursor);
    _ui->log->insertPlainText(text.append("\n"));
}

void MainWindow::loadLevel(QString text)
{
    Level * l = _ls[text];
    _level = l;
    //_ui->GLWidget->setMaze(l->_maze);
    _ui->GLWidget->setLevel(l);
    _ui->GLWidget->updateGL();
}

void MainWindow::save()
{
    int resultCode = _level->save();
    QString name = "level" + QString::number(_ls.size());
    QByteArray pack = _level->packMzlFormat();
    _ls.insert(name, _level);
    _ui->listWidget->addItem(name);
    switch(resultCode)
    {
    case -1:
        log("Error: start position wasn't set.");
        break;
    case -2:
        log("Error: end position wasn't set.");
        break;
    case -3:
        log("Error: pack can't be larger than 255 levels.");
        break;
    default:
        log((QString("Temporary file saved successfully as %1.mzl")).arg(resultCode));
    }
}

void MainWindow::pack()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save File...");
    QByteArray byteArray;
    QList<int> offsets;
    for (Level *level : _ls.values()) {
        QByteArray l = level->packMzlFormat();
        byteArray.append(l);
        offsets.push_back(l.size());
    }
    QList<int> offsetsReverse;
    offsetsReverse.reserve(offsets.size());
    std::reverse_copy(offsets.begin(), offsets.end(), std::back_inserter(offsetsReverse));

    for (int j : offsetsReverse) {
        /*for(int i = sizeof(j); i != 0; --i)
        {
          byteArray.prepend((char)(j&(0xFF << i) >>i));
        }*/
        byteArray.prepend((char)j);
    }
    byteArray.prepend((char)_ls.size());

    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    file.write(byteArray);
    file.close();
    log("File saved.");
}

void MainWindow::setWinPos(int x, int y)
{
    _level->setEnd(x,y, _level->nextId());
    _ui->GLWidget->updateGL();
}

void MainWindow::setStartPos(int x, int y)
{
    _level->setStart(x,y, _level->nextId());
    _ui->GLWidget->updateGL();
}

void MainWindow::addItem(int x, int y)
{
    if (_ui->itemsList->currentText() == "Start") {
        setStartPos(x, y);
    } else if (_ui->itemsList->currentText() == "End") {
        setWinPos(x, y);
    } else if (_ui->itemsList->currentText() == "Remove item") {
        int i = -1;
        do {
            i = _level->getItemByPos(x, y);
            if (i >= 0) {
                _level->deleteItem(i);
            }
        } while (i >= 0);
    } else if (_ui->itemsList->currentText() == "Button") {
        Item *item = new Button(x, y, _level->nextId());
        _level->addItem(item);
        item->setProps(_ui->itemProps->text());
    } else if (_ui->itemsList->currentText() == "Half Wall") {
        Item *item = new HalfWall(x, y, _level->nextId());
        _level->addItem(item);
        item->setProps(_ui->itemProps->text());
    } else if (_ui->itemsList->currentText() == "Rotated Cell") {
        Item *item = new RotatedCell(x, y, _level->nextId());
        _level->addItem(item);
        //item->setProps(_ui->itemProps->text());
    }
    _ui->GLWidget->updateGL();
    int id = _level->getLastId();
    _ui->nextItemId->setText("Last item ID: " + QString::number(id));
}

bool MainWindow::isWallsSelected()
{
    return _ui->itemsList->currentText() == "Change Walls";
}

bool MainWindow::isCellSelected()
{
    return _ui->itemsList->currentText() == "Add cell";
}

MainWindow::~MainWindow()
{
    delete _level;
    delete _ui;
}

