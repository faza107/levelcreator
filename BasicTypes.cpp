#include "BasicTypes.h"

Position::Position(int x, int y)
{
	_x = x;
	_y = y;
}

int Position::getX()
{
	return _x;
}

int Position::getY()
{
	return _y;
}

void Position::setX(int x)
{
	_x = x;
}

void Position::setY(int y)
{
	_y = y;
}

Direction oposit(Direction p)
{
    switch (p) {
    case UP:
        return DOWN;
    case DOWN:
        return UP;
    case LEFT:
        return RIGHT;
    case RIGHT:
        return LEFT;
    }
}

int plusX(int x, Direction dir)
{
    switch (dir) {
    case UP:
    case DOWN:
        return x;
    case LEFT:
        return x - 1;
    case RIGHT:
        return x + 1;
    }
}


int plusY(int y, Direction dir)
{
    switch (dir) {
    case UP:
        return y + 1;
    case DOWN:
        return y - 1;
    case LEFT:
    case RIGHT:
        return y;
    }
}

char lowByte(short a)
{
    return (unsigned char)(a & 255);
}

char hiByte(short a)
{
    return (unsigned char)(a >> 8);
}
