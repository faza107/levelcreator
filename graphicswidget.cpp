#include "graphicswidget.h"
#define PIX_PER_CELL 20

GraphicsWidget::GraphicsWidget(QWidget *parent)
    : QGLWidget(parent)
{
    mainWindow = parent->parentWidget();
}

GraphicsWidget::~GraphicsWidget()
{

}

void GraphicsWidget::initializeGL()
{
    glClearColor(0,0,0,0);
    setMouseTracking(true);
}

void GraphicsWidget::resizeGL(int width, int height)
{
    _width = width;
    _height = height;
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,width/ PIX_PER_CELL, 0, height/ PIX_PER_CELL,1,0);
}

void GraphicsWidget::paintGL()
{
    glClear(GL_COLOR_BUFFER_BIT);
    //_maze->Render();
    _level->render();
}

void GraphicsWidget::mousePressEvent(QMouseEvent *me)
{
    double realX = 1.0 * (me->x() - 4) / PIX_PER_CELL;
    double realY = 1.0 * (_height - me->y()) / PIX_PER_CELL;
    int x = realX;
    int y = realY;
    /*
    if(me->button() == Qt::RightButton)
        ((MainWindow *)mainWindow)->setWinPos(x,y);
    if(me->button() == Qt::LeftButton)
        ((MainWindow *)mainWindow)->setStartPos(x,y);
    */
    if (((MainWindow *)mainWindow)->isWallsSelected()) {
        Direction dir;
        if ((realX - x) + (realY - y) > 1) {
            if ((realX - x) > (realY - y)) {
                dir = RIGHT;
            } else {
                dir = UP;
            }
        } else {
            if ((realX - x) > (realY - y)) {
                dir = DOWN;
            } else {
                dir = LEFT;
            }
        }
        switch(dir) {
        case LEFT:
            dir = UP;
            break;
        case RIGHT:
            dir = UP;
            x++;
            break;
        case UP:
            dir = RIGHT;
            y++;
            break;
        case DOWN:
            dir = RIGHT;
            break;
        }

        int nx = plusX(x, dir);
        int ny = plusY(y, dir);
        if (_level->_maze->_maze[x][y].has(dir)) {
            _level->_maze->_maze[x][y].unbuild(dir);
            _level->_maze->_maze[nx][ny].unbuild(oposit(dir));
        } else {
            _level->_maze->_maze[x][y].build(dir);
            _level->_maze->_maze[nx][ny].build(oposit(dir));
        }
    } else if (((MainWindow *)mainWindow)->isCellSelected()) {
        _level->_maze->addCell(Position(x, y));
    } else {
        ((MainWindow *)mainWindow)->addItem(x, y);
    }
    repaint();
}

void GraphicsWidget::setMaze(Maze *maze)
{
    _maze = maze;
}

void GraphicsWidget::setLevel(Level *level)
{
    _level = level;
}
