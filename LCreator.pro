#-------------------------------------------------
#
# Project created by QtCreator 2013-12-28T22:09:07
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Triangle
TEMPLATE = app

CONFIG += c++11

SOURCES += main.cpp\
    graphicswidget.cpp \
    mainwindow.cpp \
    BasicTypes.cpp \
    BuildingPoint.cpp \
    Cell.cpp \
    Maze.cpp \
    Render.cpp \
    WallPoint.cpp \
    Level.cpp \
    items/start.cpp \
    items/end.cpp \
    items/button.cpp \
    items/halfwall.cpp \
    items/rotatedcell.cpp

HEADERS  += \
    graphicswidget.h \
    mainwindow.h \
    BasicTypes.h \
    BuildingPoint.h \
    Cell.h \
    Maze.h \
    WallPoint.h \
    Level.h \
    item.h \
    items/start.h \
    items/items.h \
    items/end.h \
    items/button.h \
    items/halfwall.h \
    items/rotatedcell.h

OTHER_FILES +=

FORMS += \
    mainwindow.ui
