#include "WallPoint.h"

WallPoint::WallPoint()
{
	_walls = 0;
	_isEmpty = 1;
	isStart = 0;
	isEnd = 0;
}

void WallPoint::build(Direction dir)
{
	_walls |= (1 << dir);
}

void WallPoint::unbuild(Direction dir)
{
	_walls &= ~(1 << dir);
}


bool WallPoint::has(Direction dir)
{
	return _walls & (1 << dir);
}

int WallPoint::isWalled()
{
	return _walls;
}

bool WallPoint::isEmpty()
{
	return _isEmpty;
}

void WallPoint::fill()
{
	_isEmpty = 0;
}

void WallPoint::empty()
{
	_isEmpty = 1;
}