#ifndef ITEM_H
#define ITEM_H

#include <QMap>
#include <QByteArray>

class Item
{
public:
    int type;
    int id;
    int _x, _y;
    Item(){}
    virtual QByteArray pack() {return NULL;}
    virtual void render() {}
    QMap<QString, void*> props;
    ~Item() {
        for (QString prop : props.keys()) {
            //delete props[prop];
        }
    }

    virtual void setProps(QString props) {

    }

};

#endif // ITEM_H
