#pragma once
#include "WallPoint.h"

class BuildingPoint
{
private:
	Position _pos;
	Direction _cameFrom;
public:
	BuildingPoint(Position &pos, Direction cameFrom);
	Position &getPosition();
	Direction &whereFrom();
};