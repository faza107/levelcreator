#ifndef START_H
#define START_H

#include "item.h"

class Start : public Item
{
public:
    Start(int x, int y, int id);
    void render();
    void addGUI();
    virtual QByteArray pack();
};

#endif // START_H
