#include "items.h"

#include <QString>
#include <QStringList>

Button::Button(int x, int y, int id)
{
    type = BUTTON_ID;
    _x = x;
    _y = y;
    this->id = id;
}

void Button::setProps(QString props)
{
    QStringList propList = props.split(",");
    for (QString prop : propList) {
        if (prop == "l") {
            dir = DIR_LEFT;
        } else if (prop == "r") {
            dir = DIR_RIGHT;
        } else if (prop == "u") {
            dir = DIR_UP;
        } else if (prop == "d") {
            dir = DIR_DOWN;
        }
    }
}

QByteArray Button::pack()
{
    QByteArray res;
    char id = this->id;
    res.append(id);
    res.append((char)type);
    short len = 3;
    res.append(hiByte(len));
    res.append(lowByte(len));
    res.append(_x);
    res.append(_y);
    res.append((char)dir);
    return res;
}
