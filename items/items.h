#ifndef ITEMS
#define ITEMS

#include "item.h"
#include "start.h"
#include "end.h"
#include "button.h"
#include "halfwall.h"
#include "BasicTypes.h"
#include "rotatedcell.h"



// types
const int START_ID        = 1;
const int END_ID          = 2;
const int BUTTON_ID       = 3;
const int HALF_WALL_ID    = 4;
const int ROTATED_CELL_ID = 5;


// directions
const int DIR_LEFT  = 0;
const int DIR_UP    = 1;
const int DIR_RIGHT = 2;
const int DIR_DOWN  = 3;


#endif // ITEMS

