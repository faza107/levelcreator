#ifndef END_H
#define END_H


#include "item.h"


class End : public Item
{
public:
    End(int x, int y, int id);
    void render();
    virtual QByteArray pack();
};

#endif // END_H
