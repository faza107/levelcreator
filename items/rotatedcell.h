#ifndef ROTATEDCELL_H
#define ROTATEDCELL_H


#include "item.h"

class RotatedCell : public Item
{
public:
    char button_id = -1;
    RotatedCell(int x, int y, int id);
    virtual void setProps(QString props);
    void render();
    void addGUI();
    virtual QByteArray pack();
};

#endif // ROTATEDCELL_H
