#include "items.h"


RotatedCell::RotatedCell(int x, int y, int id)
{
    type = ROTATED_CELL_ID;
    _x = x;
    _y = y;
    this->id = id;
}

void RotatedCell::setProps(QString props)
{
    QStringList propList = props.split(",");
    /*for (QString prop : propList) {
        if (prop == "l") {
            dir = DIR_LEFT;
        } else if (prop == "r") {
            dir = DIR_RIGHT;
        } else if (prop == "u") {
            dir = DIR_UP;
        } else if (prop == "d") {
            dir = DIR_DOWN;
        }
    }*/
    button_id = props.toInt();
}

void RotatedCell::addGUI() {

}

QByteArray RotatedCell::pack()
{
    QByteArray res;
    char id = this->id;
    res.append(id);
    res.append((char)type);
    short len = 3;
    res.append(hiByte(len));
    res.append(lowByte(len));
    res.append(_x);
    res.append(_y);
    res.append(button_id);
    return res;
}
