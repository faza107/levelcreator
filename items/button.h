#ifndef BUTTON_H
#define BUTTON_H


class Button : public Item
{
private:
    int dir = -1;
public:
    Button(int x, int y, int id);
    virtual void setProps(QString props);
    virtual void render();
    virtual QByteArray pack();
};

#endif // BUTTON_H
