#ifndef HALFWALL_H
#define HALFWALL_H

#include <QString>
#include <QByteArray>
#include <items/items.h>

class HalfWall : public Item
{
private:
    int dir = -1;
public:
    HalfWall(int x, int y, int id);
    virtual void setProps(QString props);
    virtual void render();
    virtual QByteArray pack();
};

#endif // HALFWALL_H
