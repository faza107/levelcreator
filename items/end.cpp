#include "end.h"


#include "items/items.h"

End::End(int x, int y, int id)
{
    type = END_ID;
    _x = x;
    _y = y;
    this->id = id;
}

QByteArray End::pack()
{
    QByteArray res;
    char id = this->id;
    res.append(id);
    res.append((char)type);
    short len = 2;
    res.append(hiByte(len));
    res.append(lowByte(len));
    res.append(_x);
    res.append(_y);
    return res;
}
