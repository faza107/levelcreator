#pragma once
#include <vector>
#include "BuildingPoint.h"
#include "Cell.h"

class Maze
{
public:
	int _width, _height;
	std::vector<std::vector<WallPoint>> _maze;
	std::vector<BuildingPoint> bp;
	std::vector<Cell> _cells;
	void makeBuildingStep();
	void setWinPos(Position &winPos);
	int findByPos(Position pos);

	Maze(int n);
	void Render();
	bool isWall(Position pos, Direction dir);
	void stepAt(Position pos, int step);
    void addCell(Position pos);
	~Maze();
    void addX();
    void addY();
};
