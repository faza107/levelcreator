#include <iostream>

#include <fstream>
#include "Level.h"

typedef unsigned long long ull;

using namespace std;

Level::Level(int n)
{
	if (n == 0)
		std::cin >> n;
	_maze = new Maze(n);
	_n = n;
    start = Position(-1,-1);
    end = Position(-1,-1);
}

void Level::setStart(int x, int y, int id)
{
    if (x < _maze->_width && y < _maze->_height && !_maze->_maze[x][y].isEmpty()
            && !(x == end.getX() && y == end.getY()))
	{
        if(start.getX() >= 0)
            _maze->_maze[start.getX()][start.getY()].isStart = 0;
		_maze->_maze[x][y].isStart = 1;
        start.setX(x);
        start.setY(y);
        if (s >= 0) {
            items.erase(items.begin() + s);
            s = -1;
        }
        s = items.size();
        Item *startItem = new Start(x, y, id);
        items.push_back(startItem);
	}
}

int Level::getItemByPos(int x, int y)
{
    for (int i = 0; i < items.size(); i++) {
        if (items[i]->_x == x && items[i]->_y == y)
            return i;
    }
    return -1;
}

void Level::deleteItem(int i)
{
    if (s == i) return;//s = -1;
    if (e == i) return;//e = -1;
    items.erase(items.begin() + i);
}

void Level::addItem(Item *item)
{
    items.push_back(item);
}

QByteArray Level::packMzlFormat()
{
    QByteArray result;
    char  width = 0, height = 0;
    height = _maze->_height;
	width = _maze->_width;
    
    result.append(width);
    result.append(height);
    short minSteps = wayOutSteps();
    result.append(hiByte(minSteps));
    result.append(lowByte(minSteps));
    

    /*int c = 0;
    char tmp = 0;
    for (int i = 0; i < width; i++) {
        for (int j = 0; j < height; j++, c += 2) {
            if (c % 8 == 0 && c > 0) {
                result.append(tmp);
                tmp = 0;
                c = 0;
            }
            if (_maze->_maze[i][j].has(UP)) {
                tmp |= 1 << (7 - c);
            }
            if (_maze->_maze[i][j].has(RIGHT)) {
                tmp |= 1 << (6 - c);
            }
        }
        if (c % 8 == 0 && c > 0) {
            result.append(tmp);
            tmp = 0;
            c = 0;
        }
        tmp |= 3 << (6 - c);
        c += 2;
        if (c % 8 == 0 && c > 0) {
            result.append(tmp);
            tmp = 0;
            c = 0;
        }
    }*/
    int c = 0;
    char wall = 0;
    for (int i = 0; i <= width; i++) {
        for (int j = 0; j <= height; j++) {

            if (j == height || i == width) {
                wall |= 3;
            } else {
                if (_maze->_maze[i][j].has(UP)) {
                    wall |= 1 << 1; // left
                }
                if (_maze->_maze[i][j].has(RIGHT)) {
                    wall |= 1; // down
                }
            }
            c++;
            if (c % 4 == 0 && c > 0) {
                result.append(wall);
                wall = 0;
            } else {
                wall <<= 2;
            }
        }
        //result.append((char)3);
    }
    if (wall != 0) {
        result.append(wall << (8 - 2 * (c % 4)));
    }
    /*for (int i = 0; i <= width; i++) {
        result.append((char)3);
    }*/
    result.append((char)items.size());
    QString ree(result.toHex());
    for (int i = 0; i < items.size(); i++) {
        QByteArray itemArray(items[i]->pack());
        result.append(itemArray);
    }
    QString reee(result.toHex());

    return result;
}

void Level::setEnd(int x, int y, int id)
{
    if (x < _maze->_width && y < _maze->_height && !_maze->_maze[x][y].isEmpty()
            && !(x == start.getX() && y == start.getY()))
	{
        if(end.getX() >= 0)
            _maze->_maze[end.getX()][end.getY()].isEnd = 0;
		_maze->_maze[x][y].isEnd = 1;
        end.setX(x);
        end.setY(y);
        if (e >= 0) {
            items.erase(items.begin() + e);
            e = -1;
        }
        e = items.size();
        Item *endItem = new End(x, y, id);
        items.push_back(endItem);
	}
}

char level = 1;



int Level::save()
{
    if(start.getX() == -1)
    {
        return -1;
    }
    if(end.getX() == -1)
    {
        return -2;
    }
    if(level > 255)
    {
        return -3;
    }
	string fname;
    fname = to_string(level);
	ofstream file;
	char  width = 0, height = 0;
	short minSteps = wayOutSteps();
    file.open(("Levels/" + fname + ".mzl").c_str(), ios::binary);
	height = _maze->_height;
	width = _maze->_width;
	file << width << height << hiByte(minSteps) << lowByte(minSteps);

	vector<vector<char>> walls;
	walls.resize(_maze->_width);
	for (int i = 0; i < _maze->_width; i++)
	{
		walls[i].resize(_maze->_height);
		for (int j = 0; j < _maze->_height; j++)
		{
			walls[i][j] = 0;
			if (_maze->_maze[i][j].isWalled() & (1 << UP))
				walls[i][j] |= 1 << LEFT;
			if (_maze->_maze[i][j].isWalled() & (1 << RIGHT))
				walls[i][j] |= 1 << DOWN;
			if (_maze->_maze[i + 1][j + 1].isWalled() & (1 << LEFT))
				walls[i][j] |= 1 << UP;
			if (_maze->_maze[i + 1][j + 1].isWalled() & (1 << DOWN))
				walls[i][j] |= 1 << RIGHT;
		}
	}
	for (int i = 0; i < _maze->_width; i++)
		for (int j = 0; j < _maze->_height; j++)
		{
			char cell;
			cell = walls[i][j];
			cell |= (_maze->_maze[i][j].isEmpty() << 6);
			cell |= (_maze->_maze[i][j].isStart << 5);
			cell |= (_maze->_maze[i][j].isEnd << 4);
			file << cell;
		}
	file.close();
	level++;
    return level - 1;
}

void Level::pack(string outFName)
{
	short offset = 0;
	char *data = NULL;
	ofstream packFile;
    packFile.open(outFName.c_str(), ios::binary|ios::out);
	packFile << (char)(level - 1);
	for (int i = 1; i < level; i++)
	{
		string fname;
        fname = to_string((ull)1);
		ifstream file;
		file.open(("Levels\\" + fname + ".mzl").c_str(), ios::binary|ios::ate|ios::in);
        short size = (short)file.tellg();
		file.seekg(0);
        data = (char *)realloc(data, offset + size);
		file.read(data + offset, size);
		file.close();
		packFile << hiByte(offset) << lowByte(offset);
		offset += size;
	}
	packFile.write(data, offset);
	level = 1;
	packFile.close();
}

int Level::shortestWay(vector<Position>& currentPath, int currentLength)
{
	int result = _maze->_width * _maze->_height;
	Position current = currentPath[currentPath.size() - 1];
	int is_visited = 0;
    for(unsigned int i = 0; i < currentPath.size(); i++)
	{
        Position p = currentPath[i];
		if (p.getX() == current.getX() - 1 && p.getY() == current.getY())
		{
			is_visited = 1;
			break;
		}
	}
	if (!_maze->isWall(current, LEFT) && !is_visited)
	{
		currentLength++;
		
		if (end.getX() == current.getX() - 1 && end.getY() == current.getY())
		{
			return currentLength;
		}
		currentPath.push_back(Position(current.getX() - 1, current.getY()));
		int leftRes = shortestWay(currentPath, currentLength);
		if (result > leftRes)
			result = leftRes;
		currentPath.pop_back();
		currentLength--;
	}
	is_visited = 0;
    for(unsigned int i = 0; i < currentPath.size(); i++)
    {
        Position p = currentPath[i];
		if (p.getX() == current.getX() + 1 && p.getY() == current.getY())
		{
			is_visited = 1;
			break;
		}
	}
	if (!_maze->isWall(current, RIGHT) && !is_visited)
	{
		currentLength++;

		if (end.getX() == current.getX() + 1 && end.getY() == current.getY())
		{
			return currentLength;
		}
		currentPath.push_back(Position(current.getX() + 1, current.getY()));
		int rightRes = shortestWay(currentPath, currentLength);
		if (result > rightRes)
			result = rightRes;
		currentPath.pop_back();
		currentLength--;
	}
	is_visited = 0;
    for( unsigned int i = 0; i < currentPath.size(); i++)
    {
        Position p = currentPath[i];
		if (p.getX() == current.getX() && p.getY() == current.getY() - 1)
		{
			is_visited = 1;
			break;
		}
	}
	if (!_maze->isWall(current, DOWN) && !is_visited)
	{
		currentLength++;

		if (end.getX() == current.getX() && end.getY() == current.getY() - 1)
		{
			return currentLength;
		}
		currentPath.push_back(Position(current.getX(), current.getY() - 1));
		int downRes = shortestWay(currentPath, currentLength);
		if (result > downRes)
			result = downRes;
		currentPath.pop_back();
		currentLength--;
	}
	is_visited = 0;
    for(unsigned int i = 0; i < currentPath.size(); i++)
    {
        Position p = currentPath[i];
		if (p.getX() == current.getX() && p.getY() == current.getY() + 1)
		{
			is_visited = 1;
			break;
		}
	}
	if (!_maze->isWall(current, UP) && !is_visited)
	{
		currentLength++;

		if (end.getX() == current.getX() && end.getY() == current.getY() + 1)
		{
			return currentLength;
		}
		currentPath.push_back(Position(current.getX(), current.getY() + 1));
		int upRes = shortestWay(currentPath, currentLength);
		if (result > upRes)
			result = upRes;
		currentPath.pop_back();
		currentLength--;
	}
	return result;
}

bool Level::isPacked()
{
    if(level == 1)
        return true;
    else
        return false;
}

int Level::wayOutSteps()
{
	vector<Position> currentPath;
    currentPath.push_back(start);
	return shortestWay(currentPath, 0);
}
