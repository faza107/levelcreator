
#pragma once
#include <QByteArray>
#include <string>
#include "Maze.h"
#include "items/items.h"

class Level
{
private:
	int shortestWay(std::vector<Position>& currentPath, int currentLength);
	int wayOutSteps();
    std::vector<Item*> items;
    Position start, end;
    int e = -1, s = -1;
    int lastId = 1;
public:
	Maze *_maze;
	int _n;
	Level(int n = 0);
	void setStart(int x, int y, int id);
    void setEnd(int x, int y, int id);
    int save();
    void pack(std::string outFName);
    bool isPacked();
    void render();
    int getItemByPos(int x, int y);
    void deleteItem(int i);
    void addItem(Item *item);
    QByteArray packMzlFormat();
    int nextId(){
        return ++lastId;
    }
    int getLastId() {
        return lastId;
    }
};
