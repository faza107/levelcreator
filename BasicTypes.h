#pragma once

typedef double dbl;



enum Direction
{
    LEFT = 0,
    DOWN = 1,
    UP = 2,
    RIGHT = 3
};

class Position
{
private:
	int _x, _y;
public:
	Position(int x = 0, int y = 0);
	int getX();
	int getY();
	void setX(int x);
	void setY(int y);
};

Direction oposit(Direction p);
int plusX(int x, Direction dir);
int plusY(int y, Direction dir);
char lowByte(short a);
char hiByte(short a);
