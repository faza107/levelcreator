#pragma once
#include "BasicTypes.h"

class Cell
{
public:
	int neighbours[4];
	Position _pos;
	Cell(int up, int down, int left, int right, Position &pos);
};