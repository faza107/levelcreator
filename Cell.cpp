#include "Cell.h"

Cell::Cell(int up, int down, int left, int right, Position &pos)
{
	neighbours[0] = up;
	neighbours[1] = right;
	neighbours[2] = down;
	neighbours[3] = left;
	_pos = pos;
}