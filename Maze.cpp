#include <cstdlib>
#include <ctime>
#include "Maze.h"

#define GAP 20

Maze::Maze(int n)
{
	int s = n - 1;
	srand(time(NULL));
    Position p(0, 0);
    _cells.push_back(Cell(-1, -1, -1, -1, p));
	while (s > 0)
	{
		int i = rand() % _cells.size();
		int dir = rand() % 4;
		if (_cells[i].neighbours[dir] == -1)
		{
			int up, left, down, right;
			switch (dir)
			{
			case 0: //up
				up = findByPos(Position(_cells[i]._pos.getX(), _cells[i]._pos.getY() + 2));
				left = findByPos(Position(_cells[i]._pos.getX() - 1, _cells[i]._pos.getY() + 1));
				right = findByPos(Position(_cells[i]._pos.getX() + 1, _cells[i]._pos.getY() + 1));
                p = Position(_cells[i]._pos.getX(), _cells[i]._pos.getY() + 1);
                _cells.push_back(Cell(up, i, left, right, p));
				_cells[i].neighbours[0] = _cells.size() - 1;
				if (up != -1)
					_cells[up].neighbours[2] = _cells.size() - 1;
				if (left != -1)
					_cells[left].neighbours[1] = _cells.size() - 1; 
				if (right != -1)
					_cells[right].neighbours[3] = _cells.size() - 1;
				s--;
				break;

			case 1: //right
				up = findByPos(Position(_cells[i]._pos.getX() + 1, _cells[i]._pos.getY() + 1));
				down = findByPos(Position(_cells[i]._pos.getX() + 1, _cells[i]._pos.getY() - 1));
				right = findByPos(Position(_cells[i]._pos.getX() + 2, _cells[i]._pos.getY()));
                p = Position(_cells[i]._pos.getX() + 1, _cells[i]._pos.getY());
                _cells.push_back(Cell(up, down, i, right, p));
				_cells[i].neighbours[1] = _cells.size() - 1;
				if (up != -1)
					_cells[up].neighbours[2] = _cells.size() - 1;
				if (right != -1)
					_cells[right].neighbours[3] = _cells.size() - 1;
				if (down != -1)
					_cells[down].neighbours[0] = _cells.size() - 1;
				s--;
				break;

			case 2: //down
				down = findByPos(Position(_cells[i]._pos.getX(), _cells[i]._pos.getY() - 2));
				left = findByPos(Position(_cells[i]._pos.getX() - 1, _cells[i]._pos.getY() - 1));
				right = findByPos(Position(_cells[i]._pos.getX() + 1, _cells[i]._pos.getY() - 1));
                p = Position(_cells[i]._pos.getX(), _cells[i]._pos.getY() - 1);
                _cells.push_back(Cell(i, down, left, right, p));
				_cells[i].neighbours[2] = _cells.size() - 1;
				if (left != -1)
					_cells[left].neighbours[1] = _cells.size() - 1;
				if (right != -1)
					_cells[right].neighbours[3] = _cells.size() - 1;
				if (down != -1)
					_cells[down].neighbours[0] = _cells.size() - 1;
				s--;
				break;

			case 3: //left
				up = findByPos(Position(_cells[i]._pos.getX() - 1, _cells[i]._pos.getY() + 1));
				left = findByPos(Position(_cells[i]._pos.getX() - 2, _cells[i]._pos.getY()));
				down = findByPos(Position(_cells[i]._pos.getX() - 1, _cells[i]._pos.getY() - 1));
                p = Position(_cells[i]._pos.getX() - 1, _cells[i]._pos.getY());
                _cells.push_back(Cell(up, down, left, i, p));
				_cells[i].neighbours[3] = _cells.size() - 1;
				if (up != -1)
					_cells[up].neighbours[2] = _cells.size() - 1;
				if (left != -1)
					_cells[left].neighbours[1] = _cells.size() - 1;
				if (down != -1)
					_cells[down].neighbours[0] = _cells.size() - 1;
				s--;
				break;
			}
		}
	}

	int minX = 0, maxX = 0, minY = 0, maxY = 0;
	for (int i = 1; i < _cells.size(); i++)
	{
		if (_cells[i]._pos.getX() < _cells[minX]._pos.getX())
			minX = i;
		if (_cells[i]._pos.getY() < _cells[minY]._pos.getY())
			minY = i;
		if (_cells[i]._pos.getX() > _cells[maxX]._pos.getX())
			maxX = i;
		if (_cells[i]._pos.getY() > _cells[maxY]._pos.getY())
			maxY = i;
	}

	int shiftX = -_cells[minX]._pos.getX();
	int shiftY = -_cells[minY]._pos.getY();

	_width = _cells[maxX]._pos.getX() + shiftX + 1;
	_height = _cells[maxY]._pos.getY() + shiftY + 1;
    _maze.resize(_width + 1 + GAP);
    for (int i = 0; i < _maze.size(); i++)
        _maze[i].resize(_height + 1 + GAP);

	for (int i = 0; i < _cells.size(); i++)
		_maze[_cells[i]._pos.getX() + shiftX][_cells[i]._pos.getY() + shiftY].fill();

	for (int i = 0; i < _width + 1; i++)
	{
		for (int j = 0; j < _height + 1; j++)
		{
			if (!_maze[i][j].isEmpty())
			{
				if (_maze[i + 1][j].isEmpty())
				{
					_maze[i + 1][j].build(UP);
					_maze[i + 1][j+1].build(DOWN);
					if (rand() % 4 == 0)
                    {
                        p = Position(i + 1, j);
                        bp.push_back(BuildingPoint(p, RIGHT));
                    }
                }
				if (i == 0 || (i != 0 && _maze[i - 1][j].isEmpty()))
				{
					_maze[i][j].build(UP);
					_maze[i][j + 1].build(DOWN);
					if (rand() % 4 == 0)
                    {
                        p = Position(i, j);
                        bp.push_back(BuildingPoint(p, LEFT));
                    }
                }
				if (_maze[i][j + 1].isEmpty())
				{
					_maze[i][j + 1].build(RIGHT);
					_maze[i + 1][j + 1].build(LEFT);
					if (rand() % 4 == 0)
                    {
                        p = Position(i, j + 1);
                        bp.push_back(BuildingPoint(p, UP));
                    }
                }
				if (j == 0 || (j != 0 && _maze[i][j - 1].isEmpty()))
				{
					_maze[i][j].build(RIGHT);
					_maze[i + 1][j].build(LEFT);
					if (rand() % 4 == 0)
                    {
                        p = Position(i, j);
                        bp.push_back(BuildingPoint(p, DOWN));
                    }
                }
			}
		}
	}
	int k = n / 40;
	while (k > 0)
	{
		int x = rand() % _width;
		int y = rand() % _height;
		if (!_maze[x][y].isEmpty() && !_maze[x + 1][y].isEmpty() && !_maze[x][y + 1].isEmpty() && !_maze[x + 1][y + 1].isEmpty())
		{
            Position p(x+1, y+1);
            bp.push_back(BuildingPoint(p, (Direction)(rand()%4)));
			k--;
		}
	}
	makeBuildingStep();
}

int Maze::findByPos(Position pos)
{
	for (int i = 0; i < _cells.size(); i++)
	{
		if (_cells[i]._pos.getX() == pos.getX() && _cells[i]._pos.getY() == pos.getY())
			return i;
	}
	return -1;
}

void Maze::makeBuildingStep()
{
	while (bp.size() != 0)
	{
		Position pos = bp[0].getPosition();
		int is_continued = 0, is_possible = 0;
		if (pos.getX() != 0 && (!_maze[pos.getX() - 1][pos.getY()].isEmpty() || (pos.getY() != 0 && !_maze[pos.getX() - 1][pos.getY() - 1].isEmpty()))
			&& !_maze[pos.getX() - 1][pos.getY()].isWalled())
		{
			if ((rand() % 2 == 1 && !bp[0].whereFrom() == LEFT) || (rand()%4 == 1))
			{
				_maze[pos.getX()][pos.getY()].build(LEFT);
				_maze[pos.getX() - 1][pos.getY()].build(RIGHT);
                Position p(pos.getX() - 1, pos.getY());
                bp.push_back(BuildingPoint(p, RIGHT));
				is_continued = 1;
			}
			is_possible = 1;
		}
		if (pos.getX() != _width && (!_maze[pos.getX()][pos.getY()].isEmpty() || (pos.getY() != 0 && !_maze[pos.getX()][pos.getY() - 1].isEmpty()))
			&& !_maze[pos.getX() + 1][pos.getY()].isWalled())
		{
			if ((rand() % 2 == 1 && !bp[0].whereFrom() == RIGHT) || (rand() % 4 == 1))
			{
				_maze[pos.getX()][pos.getY()].build(RIGHT);
				_maze[pos.getX() + 1][pos.getY()].build(LEFT);
                Position p(pos.getX() + 1, pos.getY());
                bp.push_back(BuildingPoint(p, LEFT));
				is_continued = 1;
			}
			is_possible = 1;
		}
		if (pos.getY() != 0 && (!_maze[pos.getX()][pos.getY() - 1].isEmpty() || (pos.getX() != 0 && !_maze[pos.getX() - 1][pos.getY() - 1].isEmpty()))
			&& !_maze[pos.getX()][pos.getY() - 1].isWalled())
		{
			if ((rand() % 2 == 1 && !bp[0].whereFrom() == DOWN) || (rand() % 4 == 1))
			{
				_maze[pos.getX()][pos.getY()].build(DOWN);
				_maze[pos.getX()][pos.getY() - 1].build(UP);
                Position p(pos.getX(), pos.getY() - 1);
                bp.push_back(BuildingPoint(p, UP));
				is_continued = 1;
			}
			is_possible = 1;
		}
		if (pos.getY() != _height && (!_maze[pos.getX()][pos.getY()].isEmpty() || (pos.getX() != 0 && !_maze[pos.getX() - 1][pos.getY()].isEmpty()))
			&& !_maze[pos.getX()][pos.getY() + 1].isWalled())
		{
			if ((rand() % 2 == 1 && !bp[0].whereFrom() == UP) || (rand() % 4 == 1))
			{
				_maze[pos.getX()][pos.getY()].build(UP);
				_maze[pos.getX()][pos.getY() + 1].build(DOWN);
                Position p(pos.getX(), pos.getY() + 1);
                bp.push_back(BuildingPoint(p, DOWN));
				is_continued = 1;
			}
			is_possible = 1;
		}
		if (is_continued || !is_possible)
			bp.erase(bp.begin());
	}
}

bool Maze::isWall(Position pos, Direction dir)
{
	switch (dir)
	{
	case LEFT:
		if (_maze[pos.getX()][pos.getY()].has(UP))
			return 1;
			break;
	case RIGHT:
		if (_maze[pos.getX() + 1][pos.getY()].has(UP))
			return 1;
		break;
	case UP:
		if (_maze[pos.getX()][pos.getY() + 1].has(RIGHT))
			return 1;
		break;
	case DOWN:
		if (_maze[pos.getX()][pos.getY()].has(RIGHT))
			return 1;
		break;
	}
    return 0;
}

void Maze::addCell(Position pos)
{
    int minX = 0, maxX = 0, minY = 0, maxY = 0;
    /*for (int i = 1; i < _cells.size(); i++)
    {
        if (_cells[i]._pos.getX() < _cells[minX]._pos.getX())
            minX = i;
        if (_cells[i]._pos.getY() < _cells[minY]._pos.getY())
            minY = i;
        if (_cells[i]._pos.getX() > _cells[maxX]._pos.getX())
            maxX = i;
        if (_cells[i]._pos.getY() > _cells[maxY]._pos.getY())
            maxY = i;
    }*/
    int x = pos.getX() + _cells[minX]._pos.getX();
    int y = pos.getY() + _cells[minY]._pos.getY();

    int up    = findByPos(Position(x, y + 1));
    int down  = findByPos(Position(x, y - 1));
    int left  = findByPos(Position(x - 1, y));
    int right = findByPos(Position(x + 1, y));
    Cell newCell(up, down, left, right, pos);
    //_cells.push_back(newCell);
    if (x >= _width) {
        _width = x + 1;
    }
    if (y >= _height) {
        _height = y + 1;
    }
    _maze[x][y].fill();
    /*if (up != -1) {
        _maze[x][y].build(UP);
        _maze[x][y + 1].build(DOWN);
    }
    if (down != -1) {
        _maze[x][y].build(DOWN);
        _maze[x][y - 1].build(UP);
    }
    if (right != -1) {
        _maze[x][y].build(RIGHT);
        _maze[x + 1][y].build(LEFT);
    }
    if (left != -1) {
        _maze[x][y].build(LEFT);
        _maze[x - 1][y].build(RIGHT);
    }*/
}

void Maze::addX()
{
    _maze.resize(_maze.size() + 1);
}

void Maze::addY()
{
    int p = _maze.size();
    for (int i = 0; i < p; i++) {
        _maze[i].push_back(WallPoint());
    }
}


