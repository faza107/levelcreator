#include "BuildingPoint.h"

BuildingPoint::BuildingPoint(Position &pos, Direction cameFrom)
{
	_pos = pos;
	_cameFrom = cameFrom;
}

Position &BuildingPoint::getPosition()
{
	return _pos;
}

Direction &BuildingPoint::whereFrom()
{
	return _cameFrom;
}